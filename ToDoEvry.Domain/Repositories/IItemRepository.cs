﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ToDoEvry.Domain.Models;

namespace ToDoEvry.Domain.Repositories
{
    public interface IItemRepository
    {
        Task<IEnumerable<Item>> GetItems();
        Task<bool> Save(Item item);
    }

    interface IMapApi
    {
        double GetLogitude(string adress);
    }
}
