﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ToDoEvry.Domain.Models;

namespace ToDoEvry.Domain.Repositories.Implementation
{
    public class ItemRepository : IItemRepository
    {
        private readonly ToDoContext _context;

        public ItemRepository(ToDoContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Item>> GetItems()
        {
            return await _context.Items.ToListAsync();
        }

        public async Task<bool> Save(Item item)
        {
            if (item.Id == 0)
            {
                _context.Items.Add(item);

            }
            else
            {
                _context.Items.Update(item);
            }
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
