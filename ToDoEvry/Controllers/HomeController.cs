﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoEvry.Domain.Models;
using ToDoEvry.Domain.Repositories;
using ToDoEvry.Models;

namespace ToDoEvry.Controllers
{
    public class HomeController : Controller
    {
        private readonly IItemRepository _itemRepo;

        public HomeController(IItemRepository itemRepo)
        {
            _itemRepo = itemRepo;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult CreateToDo()
        {
            ViewData["Message"] = "Create ToDos.";
            return View();
        }

        [HttpPost]
        public IActionResult CreateToDo(Item item)
        {
            //ViewData["Message"] = "Create ToDos.";
            _itemRepo.Save(item);
            return RedirectToAction(
                "Index", "Item");
        }

        public IActionResult ReadToDo()
        {
            ViewData["Message"] = "Read ToDos.";
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
