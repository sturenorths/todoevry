﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoEvry.Domain.Repositories;

namespace ToDoEvry.Controllers
{
    public class ItemController : Controller
    {
        private readonly IItemRepository _itemRepo;

        public ItemController(IItemRepository itemRepo)
        {
            _itemRepo = itemRepo;
        }


        public async Task<IActionResult> Index()
        {
            var result = await _itemRepo.GetItems();
            return View(result);
        }
    }
}